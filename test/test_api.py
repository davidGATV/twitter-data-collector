from managers.data_manager import DataManager


date_since: str = "2020-07-26"
date_until: str = "2020-08-01"
search_term: list = ["#covid19", "#stayhome", "#staysafe",
                     "#stayhealthy", "#coronavirus", "#deescalation"]
lang: str = 'en'
query: str = " OR ".join(search_term)
max_tweets: int = 10

dm: DataManager = DataManager()
dm.twitter_api_manager.set_up_twitter_api_connection()


# 1. Call searching function
dm.generate_dataset_from_historical_tweets(
        date_since=date_since,
        date_until=date_until,
        search_term=search_term,
        max_tweets=max_tweets,
        lang=lang)

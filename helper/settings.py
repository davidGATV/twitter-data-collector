import os
import coloredlogs, logging

# Create a logger object.
logger = logging.getLogger(__name__)
coloredlogs.install(level='DEBUG', logger=logger)


# ================================================================
# MONGODB Params
db_host: str = os.getenv("MONGODB_HOST") if "MONGODB_HOST" in os.environ else "localhost"
db_port: str = os.getenv("MONGODB_PORT") if "MONGODB_PORT" in os.environ else "27017"
db_username: str = os.getenv("MONGODB_USERNAME") if "MONGODB_USERNAME" in os.environ else ""
db_password: str = os.getenv("MONGODB_PASSWORD") if "MONGODB_PASSWORD" in os.environ else ""
db_name: str = os.getenv("MONGODB_DB_NAME") if "MONGODB_DB_NAME" in os.environ else "twitter_data"
tweets_collection_name: str = os.getenv("TWEETS_COLLECTION_NAME") if "TWEETS_COLLECTION_NAME" in\
                                                               os.environ else "tweets"
users_collection_name: str = os.getenv("USERS_COLLECTION_NAME") if "USERS_COLLECTION_NAME" in\
                                                               os.environ else "user_accounts"

# ================================================================
# Twitter Params
consumer_key: str = os.getenv("CONSUMER_KEY") if "CONSUMER_KEY" in os.environ \
    else "WGAjibhOLOD81TcmvcIefaslu"
consumer_secret: str = os.getenv("CONSUMER_SECRET") if "CONSUMER_SECRET" in os.environ \
    else "gUZUbvHzATY7ywtjrvO4f2MhecJqfFjYN8cojz6cawf2v4W3GT"
access_token = os.getenv("ACCESS_TOKEN") if "ACCESS_TOKEN" in os.environ \
    else "973174862159253505-9bIZoFaZ4yerKQsIF2rILWIEYFmErDQ"
access_token_secret: str = os.getenv("ACCESS_TOKEN_SECRET") if "ACCESS_TOKEN_SECRET" in os.environ \
    else "O1aptMSeRFPizTUiswcW5HW1AYP2aZkvfNuCoDqUhYXpO"

# ================================================================
http_response_500: str = "Internal Server Error"
http_response_200: str = "Successful Operation"
http_response_422: str = "Invalid Input"
http_response_400: str = "Bad Request"
http_response_403: str = "HTTP Connection Error"

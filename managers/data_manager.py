from helper.settings import (logger, db_host, db_port, db_username,
                             db_password, db_name, tweets_collection_name,
                             users_collection_name)
from managers.twitter_api_manager import TwitterAPIManager
from managers.mongodb_manager import MongoDBManager
from pymongo.cursor import Cursor


class DataManager(object):
    def __init__(self):
        self.twitter_api_manager: TwitterAPIManager = TwitterAPIManager()
        self.mongodb_manager: MongoDBManager = MongoDBManager(
            host=db_host,
            port=db_port,
            username=db_username,
            password=db_password,
            db_name=db_name
        )
        self.maximum_items: int = 500

    def generate_dataset_from_historical_tweets(self, date_since: str, date_until: str, search_term: list,
                                                lang: str = "en", max_tweets: int = 500):
        try:
            logger.info("Verifying connection with both Twitter and MongoDB")

            # 1. Verify connection
            if self.twitter_api_manager.api is None:
                self.twitter_api_manager.set_up_twitter_api_connection()
            if self.mongodb_manager.db is None:
                self.mongodb_manager.set_up_db()

            # 2. Process data
            logger.info("Processing tweets & users ... ")
            for data in self.twitter_api_manager.process_batch_historical_tweets(
                    date_since=date_since,
                    date_until=date_until,
                    search_term=search_term,
                    max_tweets=max_tweets,
                    lang=lang):

                # 3. Ingest into mongoDB
                logger.info("Ingesting data into MongoDB")
                self.ingest_data_into_mongodb(data=data)
        except Exception as e:
            logger.error(e)

    def verify_documents_in_mongodb(self, entity: dict,
                                    collection_name: str):
        not_exist: bool = True
        try:

            filter_data: dict = {"id": entity.__getattribute__("id")}

            # 1. Find document
            res_doc: Cursor = self.mongodb_manager.find_document_by_filter(
                collection_name=collection_name, filter=filter_data)
            not_exist: bool = True if len(list(res_doc)) == 0 else False
        except Exception as e:
            logger.error(e)
        return not_exist

    def ingest_data_into_mongodb(self, data: zip):
        try:
            for tweet, user in list(data):

                # If the tweet and user are not in the collection db
                if self.verify_documents_in_mongodb(
                        entity=tweet, collection_name=tweets_collection_name):
                    self.mongodb_manager.insert_document_to_collection(
                        collection_name=tweets_collection_name, document=tweet.__dict__)

                if self.verify_documents_in_mongodb(
                        entity=user, collection_name=users_collection_name):
                    self.mongodb_manager.insert_document_to_collection(
                        collection_name=users_collection_name, document=user.__dict__)

        except Exception as e:
            logger.error(e)

